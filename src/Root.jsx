import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { ConnectedFlagsProvider } from 'flag';
import { ApolloProvider } from 'react-apollo';

import App from './App';

class Root extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    client: PropTypes.object.isRequired,
  };

  render() {
    return (
      <Provider store={this.props.store}>
        <ConnectedRouter history={this.props.history}>
          <ConnectedFlagsProvider>
            <ApolloProvider client={this.props.client}>
              <App />
            </ApolloProvider>
          </ConnectedFlagsProvider>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default Root;
