import React from "react";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { connect } from "react-redux";
import { message, Button, Input } from "antd";

import Message from "../components/message";

const ROOM_QUERY = gql`
  query Room($room: String!) {
    room(slug: $room) {
      id
      messages {
        id
        content
        user {
          id
          username
        }
        createdAt
      }
    }
  }
`;

class RoomContent extends React.Component {
  state = {
    messages: [],
    message: "",
  };

  componentDidMount() {
    this.initWS();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.room !== this.props.match.params.room) {
      this.ws.close();
      this.setState({ messages: [] });
      this.initWS();
    }
  }

  componentWillUnmount() {
    this.ws.close();
  }

  initWS = () => {
    fetch(`https://${this.props.host}/jwt/`, {
      method: "post",
      body: '{"user_id": 1}',
    })
      .then((response) => {
        if (response.status !== 200) {
          console.log("Error getting token");
          return;
        }

        response.json().then((data) => {
          this.ws = new WebSocket(
            `wss://${this.props.host}/ws/chat/${this.props.match.params.room}/?Bearer=${
              data.token
            }`,
          );
          this.ws.onclose = () => {
            message.error("WebSocket connection lost. Please reload.");
          };
          this.ws.onmessage = ({ data }) =>
            this.setState(({ messages }) => ({
              messages: [...messages, JSON.parse(data)],
            }));
        });
      })
      .catch((err) => console.log("Fetch Error", err));
  };

  handleChange = (e) => {
    this.setState({ message: e.target.value });
  };

  handleSubmit = (e) => {
    this.ws.send(JSON.stringify({ message: this.state.message }));
    this.setState({ message: "" });
  };

  render() {
    const room = this.props.match.params.room;
    return (
      <Query query={ROOM_QUERY} variables={{ room }} fetchPolicy="cache-and-network">
        {({ loading, error, data }) => {
          if (loading) return "Loading messages...";
          if (error) return "Error :(";

          return (
            <React.Fragment>
              {data.room.messages
                .concat(this.state.messages)
                .map(({ id, content, user: { username }, createdAt }) => (
                  <Message
                    key={id}
                    content={content}
                    user={username}
                    sent={createdAt}
                  />
                ))}

              <Input.TextArea
                value={this.state.message}
                onChange={this.handleChange}
                placeholder="Your message"
              />
              <Button onClick={this.handleSubmit}>Send</Button>
            </React.Fragment>
          );
        }}
      </Query>
    );
  }
}

export default connect((state) => ({
  token: state.flags.configs.token,
  host: state.flags.configs.backend,
}))(RoomContent);
