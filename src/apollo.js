import ApolloClient from "apollo-boost";

export default (store) => {
  const state = store.getState();

  const client = new ApolloClient({
    uri: `https://${state.flags.configs.backend}/graphql/`,
  });

  return client;
};
