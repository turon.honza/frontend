import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'antd';

class Message extends React.Component {
  static propTypes = {
    content: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    sent: PropTypes.string.isRequired,
  };

  render() {
    const date = new Date(this.props.sent);
    return (
      <Card
        style={{ width: 400, margin: 20 }}
        title={this.props.user}
        extra={date.toLocaleString()}>
        {this.props.content}
      </Card>
    );
  }
}

export default Message;
