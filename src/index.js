import React from 'react';
import ReactDOM from 'react-dom';

import Root from './Root';
import apollo from './apollo';
import configureStore from './configureStore';
import registerServiceWorker from './registerServiceWorker';

import 'antd/dist/antd.css';

const { history, store } = configureStore();
const client = apollo(store);

ReactDOM.render(
  <Root store={store} history={history} client={client} />,
  document.getElementById('root'),
);

registerServiceWorker();
